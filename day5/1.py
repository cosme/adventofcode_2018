import sys, re
from string import ascii_lowercase
file = open("input.txt", "r")

def process(l):
    prev = 0
    line = list(l)
    i=1
    while i<len(line):
        #print(i, "".join(line))
        samecaps=(line[i-1].upper() == line[i].upper())
        different=(line[i-1] != line[i])
        if samecaps & different:
            del(line[i])
            del(line[i-1])
            i-=2
        else:
            i+=1
    return "".join(line)


for line in file:
    res = process(line)
    print(len(res))

for c in ascii_lowercase:
    str = re.sub(c, '', res, flags=re.IGNORECASE)
    print(c, len(process(str)))