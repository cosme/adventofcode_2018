import sys
file = open("input.txt", "r")

def checkletters(input):
    c = {}
    has2 = False
    has3 = False
    for l in input:
        if l in c:
            c[l]+=1
        else:
            c[l]=1
    for letter,count in c.items():
        if count == 2:
            has2 = True
        if count == 3:
            has3 = True
    return(has2, has3)

nb2=0
nb3=0
for line in file:
    (has2, has3) = checkletters(line)
    print (has2, has3)
    if has2:
        nb2+=1
    if has3:
        nb3+=1
    
print (nb2*nb3)
