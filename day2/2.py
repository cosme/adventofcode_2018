file = open("input.txt", "r")
l=[]
for line in file:
    l.append(line)

def iso(a,b):
    error = 0
    for i in range(26):
        if a[i] != b[i]:
            error+=1
            if error > 1:
                return False
    return True

def findiso():
    for i in range(len(l)):
        ref=l[i]
        for j in range(i+1,len(l)):
            if iso(ref,l[j]):
                return (ref,l[j])

print (findiso())
