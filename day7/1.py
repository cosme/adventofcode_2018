import re
file = open("input.txt", "r")


def parse(s):
    ret = {}
    (ret['pre'], ret['step']) = re.search(
        "Step ([A-Z]).*([A-Z]) can begin.", s).groups()
    return ret


res = {}
done = set()
undone = set()

for line in file:
    ret = parse(line)
    if ret['step'] not in res:
        res[ret['step']] = {ret['pre']}
    else:
        res[ret['step']].add(ret['pre'])
    undone.add(ret['pre'])
    undone.add(ret['step'])


# print(res)
# print(undone)
doable = []


def do(j):
    global done, undone
    done.add(j)
    undone.discard(j)
    for i in res:
        res[i].discard(j)


def loop():
    global doable, done, undone
    canbedone = set()
    for i in undone:
        if i not in res or len(res[i]) == 0:
            canbedone.add(i)
    #doable += sorted(canbedone)
    #for j in canbedone:
    #    do(j)
    next=sorted(canbedone)[0]
    doable+=next
    do(next)
    print(undone)
    print(doable)


while len(undone) > 0:
    loop()


print(''.join(doable))
