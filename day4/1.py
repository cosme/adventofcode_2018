import re
file = open("input.txt", "r")

def parse(s):
    ret={}
    (ret['min'],ret['action']) = re.search("([0-9][0-9])\] ([^0-9]*[0-9]+|f|w)",s).groups()
    if ret['action'] not in ('f','w'):
        ret['action'] = int(re.search("([0-9]+)",ret['action']).group(0))
    ret['min'] = int(ret['min'])
    return ret

#[1518-03-01 23:58] Guard #179 begins shift
#[1518-03-02 00:27] falls asleep
#[1518-03-02 00:32] wakes up


def process(line):
    global guard, start
    if line['action'] == 'f':
        start = line['min']
    elif line['action'] == 'w':
        for m in range(start, line['min']):
            if m in guards[guard]:
                guards[guard][m]+=1
            else:
                guards[guard][m]=1
            if guard in total:
                total[guard]+=1
            else:
                total[guard]=1
    else:
        guard = line['action']
        if guard not in guards:
            guards[guard] = {}

total={}
guard = 0
start = 0
guards={}

for line in file:
    process(parse(line))



gmax = max(total, key=total.get)
print(gmax)

print(guards[gmax])

mmax = max(guards[gmax], key=guards[gmax].get)
print(mmax)

print("part 1 : ", gmax*mmax)

maxpargarde={}
for g in guards:
    if len(guards[g]) > 0:
        maxpargarde[g] = guards[g][max(guards[g], key=guards[g].get)]
        print ("garde ",g, " / max ", maxpargarde[g])
        #print(guards[g])

globalmax = max(maxpargarde, key=maxpargarde.get)
print(guards[179])