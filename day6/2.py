import re
file = open("input.txt", "r")


def parse(s):
    ret = {}
    (ret['x'], ret['y']) = (int(i)
                            for i in re.search("([0-9]+)[^0-9]+([0-9]+)", s).groups())
    ret['x'] -= 41
    ret['y'] -= 43
    ret['id'] = ret['x']*1000+ret['y']
    ret['count'] = 0
    return ret


xmax = 312
ymax = 315

places = {}
for line in file:
    ret = parse(line)
    places[ret['id']] = ret
# places[0]={}
# places[0]['count']=0
# print(places)


def distance(a, b):
    return abs(a['x']-b['x'])+abs(a['y']-b['y'])


zone = 0


def total(x, y):
    coords = {}
    coords['x'] = x
    coords['y'] = y
    dist = 0
    for p in places:
        dist += distance(places[p], coords)
        if dist >= 10000:
            # print('over')
            return 0
   # print(closest,' ',dmin)
    print(x, '/', y, ' : ', dist)
    return 1


ext = set()
for x in range(xmax):
    for y in range(ymax):
        zone += total(x, y)


print(zone)
