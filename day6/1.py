import re
file = open("input.txt", "r")


def parse(s):
    ret = {}
    (ret['x'], ret['y']) = (int(i)
                            for i in re.search("([0-9]+)[^0-9]+([0-9]+)", s).groups())
    ret['x'] -= 41
    ret['y'] -= 43
    ret['id'] = ret['x']*1000+ret['y']
    ret['count'] = 0
    return ret


xmax = 312
ymax = 315

places = {}
for line in file:
    ret = parse(line)
    places[ret['id']] = ret
# places[0]={}
# places[0]['count']=0
print(places)


def distance(a, b):
    return abs(a['x']-b['x'])+abs(a['y']-b['y'])


def closest(x, y):
    coords = {}
    coords['x'] = x
    coords['y'] = y
    closest = 0
    dmin = 100000000
    for p in places:
        dist = distance(places[p], coords)
        if dist < dmin:
            closest = p
            dmin = dist
        elif dist == dmin:
            closest = 0
   # print(closest,' ',dmin)
    if closest != 0:
        places[closest]['count'] += 1
    return closest


ext = set()
for x in range(xmax):
    for y in range(ymax):
        res = closest(x, y)
        if (x == 0) or (x == xmax) or (y == 0) or (y == ymax):
            ext.add(res)

print(places)
print(ext)

cmin = 0
pmax = -1
for p in places:
    if p not in ext:
        if places[p]["count"] > cmin:
            cmin = places[p]['count']
            pmax = p

print(pmax)
print(cmin)
