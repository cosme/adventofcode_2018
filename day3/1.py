import re
file = open("input.txt", "r")

def parse(s):
    ret={}
    (ret['id'],ret['x'],ret['y'],ret['L'],ret['H']) = (int(i) for i in re.search("([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9]+)",s).groups())
    return ret

whole = set()
double = set()

def paint(fab):
    for i in range(fab['x'], fab['x']+fab['L']):
        for j in range(fab['y'], fab['y']+fab['H']):
            rect = i*10000+j
            if rect in whole:
                # conflit
                double.add(rect)
            else:
                whole.add(rect)

def paintfull(fab):
    for i in range(fab['x'], fab['x']+fab['L']):
        for j in range(fab['y'], fab['y']+fab['H']):
            rect = i*10000+j
            if rect in double:
                # conflit
                return False
    print(fab)
    return True


for line in file:
    paint(parse(line))

print(len(double))
file = open("input.txt", "r")
for line in file:
    paintfull(parse(line))
